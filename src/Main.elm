module Main exposing (main)

import Browser
import Game exposing (..)
import View exposing (view)


main : Program () Model Msg
main =
    Browser.element
        { init = always ( { game = [], hover = Nothing }, Cmd.none )
        , view = view
        , update = Debug.log "up" >> update
        , subscriptions = always Sub.none
        }
