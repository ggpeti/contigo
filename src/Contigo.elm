module Contigo exposing (Board, Color(..), Coord, Game, Group, Move(..), aliveGroup, aliveStone, board, colored, removeDead, touches, tryMove, whoseTurn)

import Basics.Extra exposing (fractionalModBy)
import Interval exposing (Interval, endpoints, hull, intersects)
import List.Extra exposing (indexedFoldl)


type Color
    = B
    | W


type alias Coord =
    ( Float, Float )


type Move
    = Put Coord
    | Captured Coord
    | Pass


type alias Game =
    List Move


type alias Group =
    List Coord


type alias Board =
    ( List Group, List Group )


board : Game -> Board
board game =
    colored game |> Tuple.mapBoth groups groups


colored : Game -> ( List Coord, List Coord )
colored =
    indexedFoldl
        (\index elem agg ->
            case elem of
                Put coord ->
                    if (index |> remainderBy 2) == 0 then
                        ( coord :: Tuple.first agg, Tuple.second agg )

                    else
                        ( Tuple.first agg, coord :: Tuple.second agg )

                _ ->
                    agg
        )
        ( [], [] )


groups : List Coord -> List Group
groups =
    List.foldl
        (\coord agg ->
            let
                ( touching, nontouching ) =
                    agg |> List.partition (List.any (touches coord))
            in
            (coord :: List.concat touching) :: nontouching
        )
        []


touches : Coord -> Coord -> Bool
touches c1 c2 =
    distance c1 c2 < sqrt 2


distance : Coord -> Coord -> Float
distance ( x0, y0 ) ( x1, y1 ) =
    sqrt <| (x1 - x0) ^ 2 + (y1 - y0) ^ 2


tryMove : Move -> Game -> Maybe Game
tryMove move game =
    if game |> List.any (conflicts move) then
        Nothing

    else
        let
            newGame =
                game ++ [ move ]

            newBoard =
                board newGame

            removedBoard =
                removeDead (whoseTurn newGame) newBoard

            selfRemovedBoard =
                removeDead (whoseTurn game) removedBoard
        in
        if removedBoard /= selfRemovedBoard then
            Nothing

        else if newBoard == removedBoard then
            Just newGame

        else
            newGame
                |> List.indexedMap
                    (\i m ->
                        case m of
                            Put coord ->
                                if
                                    List.member coord
                                        (removedBoard
                                            |> (if (i |> remainderBy 2) == 0 then
                                                    Tuple.first

                                                else
                                                    Tuple.second
                                               )
                                            |> List.concat
                                        )
                                then
                                    m

                                else
                                    Captured coord

                            _ ->
                                m
                    )
                |> Just


whoseTurn : Game -> Color
whoseTurn game =
    if (List.length game |> remainderBy 2) == 0 then
        B

    else
        W


conflicts : Move -> Move -> Bool
conflicts m0 m1 =
    case ( m0, m1 ) of
        ( Put c0, Put c1 ) ->
            distance c0 c1 < 1

        _ ->
            False


circleInterval : Float -> Float -> List (Interval Float)
circleInterval a0 a1 =
    let
        ( a0_, a1_ ) =
            ( fractionalModBy (1 |> turns) a0, fractionalModBy (1 |> turns) a1 )
    in
    if a0_ > a1_ then
        [ Interval.from 0 a1_, Interval.from a0_ (1 |> turns) ]

    else
        [ Interval.from a0_ a1_ ]


intersectionIntervals : Coord -> Float -> Coord -> List (Interval Float)
intersectionIntervals ( x0, y0 ) r ( x1, y1 ) =
    let
        d =
            distance ( x0, y0 ) ( x1, y1 )

        a0 =
            atan2 (y1 - y0) (x1 - x0)

        l =
            (d ^ 2 - 1.0 + r ^ 2) / (2 * d)

        a =
            acos (l / r)
    in
    if isNaN a then
        []

    else
        circleInterval (a0 - a) (a0 + a)


near : Float -> Float -> Bool
near x y =
    x < y + 0.0001 && x > y - 0.0001


aliveAround : Board -> Coord -> Float -> Bool
aliveAround board_ coord radius =
    board_
        |> (\( a, b ) -> List.concat (a ++ b))
        |> List.filter (distance coord >> (>) (sqrt 2 + 1))
        |> List.filter (\( x, y ) -> not (near x (Tuple.first coord) && near y (Tuple.second coord)))
        |> List.map (intersectionIntervals coord radius)
        |> List.concat
        |> union
        |> (\is ->
                case List.map endpoints is of
                    [ ( x, y ) ] ->
                        not (near x 0 && near y (1 |> turns))

                    _ ->
                        True
           )


aliveStone : Board -> Coord -> Bool
aliveStone board_ coord =
    List.range 0 10
        |> List.map (\i -> toFloat i * (sqrt 2.0 - 1.0) / 10.0 + 1.0)
        |> List.any (aliveAround board_ coord)


aliveGroup : Board -> Group -> Bool
aliveGroup board_ group =
    List.any (aliveStone board_) group


removeDead : Color -> Board -> Board
removeDead color ( bGroups, wGroups ) =
    List.filter (aliveGroup ( bGroups, wGroups ))
        |> (\f ->
                if color == B then
                    ( f bGroups, wGroups )

                else
                    ( bGroups, f wGroups )
           )


union : List (Interval Float) -> List (Interval Float)
union =
    List.foldl
        (\i acc ->
            case List.partition (intersects i) acc of
                ( [], _ ) ->
                    i :: acc

                ( isi, iso ) ->
                    List.foldl hull i isi :: iso
        )
        []
