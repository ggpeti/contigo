module Game exposing (Model, Msg(..), update)

import Contigo exposing (..)


type alias Model =
    { game : Game
    , hover : Maybe Coord
    }


type Msg
    = Move Move
    | Hover (Maybe Coord)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Move move ->
            case Contigo.tryMove move model.game of
                Just game_ ->
                    ( { model | game = game_ }, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )

        Hover hover ->
            ( { model | hover = hover }, Cmd.none )
