module View exposing (view)

import Color exposing (black, white, yellow)
import Contigo exposing (..)
import Game exposing (..)
import Html exposing (..)
import Html.Attributes as HA
import Html.Events.Extra.Mouse exposing (Event, onLeave, onMove, onUp)
import List.Extra exposing (uniquePairs)
import TypedSvg exposing (..)
import TypedSvg.Attributes exposing (..)
import TypedSvg.Types exposing (Fill(..), Length(..), Opacity(..))


view : Model -> Html Msg
view model =
    let
        f =
            \{ offsetPos } -> ( Tuple.first offsetPos / 25.0, Tuple.second offsetPos / 25.0 )
    in
    svg
        [ HA.width 500
        , HA.height 500
        , onUp (f >> Put >> Move)
        , onMove (f >> Just >> Hover)
        , onLeave (always (Hover Nothing))
        ]
        [ viewGame model.game
        , viewHover (whoseTurn model.game)
            (model.game
                |> colored
                |> (if whoseTurn model.game == B then
                        Tuple.first

                    else
                        Tuple.second
                   )
            )
            model.hover
        ]


scale : Float
scale =
    5.0


viewGame : Game -> Html msg
viewGame game =
    board game
        |> Tuple.mapBoth (List.map (viewGroup B)) (List.map (viewGroup W))
        |> (\( b, w ) ->
                viewBackground :: (b ++ w)
           )
        |> g []


viewHover : Color -> Group -> Maybe Coord -> Html msg
viewHover color stones hover =
    case hover of
        Just coord ->
            g [ opacity (Opacity 0.7) ] ([ viewStone color coord ] ++ viewConnections color (coord :: stones))

        Nothing ->
            text ""


viewGroup : Color -> Group -> Html msg
viewGroup color group =
    g [] ((group |> List.map (viewStone color)) ++ viewConnections color group)


viewStone : Color -> Coord -> Html msg
viewStone color ( x, y ) =
    circle [ cx (Percent (scale * x)), cy (Percent (scale * y)), r (Percent (scale * 0.5)), fill (Fill (toSvgColor color)), noPointer ] []


viewBackground : Html msg
viewBackground =
    rect [ x (Percent 0), y (Percent 0), width (Percent 100), height (Percent 100), fill (Fill yellow) ] []


viewConnections : Color -> Group -> List (Html msg)
viewConnections color group =
    uniquePairs group
        |> List.filter (\( a, b ) -> touches a b)
        |> List.map
            (\( ( x1v, y1v ), ( x2v, y2v ) ) ->
                line
                    [ x1 (Percent (x1v * scale))
                    , y1 (Percent (y1v * scale))
                    , x2 (Percent (x2v * scale))
                    , y2 (Percent (y2v * scale))
                    , noPointer
                    , stroke (toSvgColor color)
                    , strokeWidth (Percent 1)
                    ]
                    []
            )


noPointer : Html.Attribute msg
noPointer =
    HA.style "pointer-events" "none"


toSvgColor : Color -> Color.Color
toSvgColor color =
    if color == B then
        black

    else
        white
