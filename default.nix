{ pkgs ? import <nixpkgs> {}, ...}:
pkgs.stdenv.mkDerivation {
  name = "contigo-env";
  src = "./.";
  buildInputs = [ pkgs.elmPackages.elm ];
}
